FROM python:3.9

WORKDIR /code/

RUN pip install --upgrade pip
RUN pip install mlflow python-dotenv fastapi Pillow typer pathlib uvicorn boto3 psycopg2 python-multipart
RUN pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cpu

COPY ./inference.py /code/app/inference.py
COPY ./unet.py /code/unet.py
COPY ./.env /code/app/.env

CMD ["uvicorn", "app.inference:app", "--host", "0.0.0.0", "--port", "80"]

To run the inference you should get all the services from the main repo. After that you could run the command:

`python get_model_output.py plane.jpg`

Output would be saved to the file `output.png`

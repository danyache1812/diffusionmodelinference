import torch
import torch.nn as nn


class UnetConvBlock(nn.Module):
    def __init__(self, in_c, out_c):
        super().__init__()

        self.conv0 = nn.Conv2d(in_c, out_c, kernel_size=3, padding=1)
        self.bn0 = nn.BatchNorm2d(out_c)

        self.conv1 = nn.Conv2d(out_c, out_c, kernel_size=3, padding=1)
        self.bn1 = nn.BatchNorm2d(out_c)

        self.conv2 = nn.Conv2d(out_c, out_c, kernel_size=3, padding=1)
        self.bn2 = nn.BatchNorm2d(out_c)

        self.relu = nn.ReLU()

    def conv(self, inputs, conv, bn, relu, residual=False):
        x = conv(inputs)
        x = bn(x)
        x = relu(x)
        if residual:
            x = x + inputs
        return x

    def forward(self, inputs):
        x0 = self.conv(inputs, self.conv0, self.bn0, self.relu, False)

        x1 = self.conv(x0, self.conv1, self.bn1, self.relu, True)

        x2 = self.conv(x1, self.conv2, self.bn2, self.relu, True)

        return x2


class EncoderBlock(nn.Module):
    def __init__(self, in_c, out_c):
        super().__init__()

        self.conv = UnetConvBlock(in_c, out_c)
        self.pool = nn.MaxPool2d((2, 2))

    def forward(self, inputs):
        x = self.conv(inputs)
        p = self.pool(x)

        return x, p


class DecoderBlock(nn.Module):
    def __init__(self, in_c, out_c):
        super().__init__()

        self.up = nn.ConvTranspose2d(in_c, out_c, kernel_size=2, stride=2, padding=0)
        self.conv = UnetConvBlock(out_c + out_c, out_c)

    def forward(self, inputs, skip):
        x = self.up(inputs)
        x = torch.cat([x, skip], axis=1)  # type: ignore
        x = self.conv(x)

        return x


class ResidualUnet(nn.Module):
    def __init__(self, out_size=3):
        super().__init__()

        # Encoder
        self.e1 = EncoderBlock(3, 64)
        self.e2 = EncoderBlock(64, 128)
        self.e3 = EncoderBlock(128, 256)
        self.e4 = EncoderBlock(256, 512)

        # Bottleneck
        self.b = UnetConvBlock(512, 1024)

        # Decoder
        self.d1 = DecoderBlock(1024, 512)
        self.d2 = DecoderBlock(512, 256)
        self.d3 = DecoderBlock(256, 128)
        self.d4 = DecoderBlock(128, 64)

        # Classifier
        self.outputs = nn.Conv2d(64, out_size, kernel_size=1, padding=0)

    def forward(self, inputs):
        # p0 = self.input_encoder(inputs)

        # Encoder
        s1, p1 = self.e1(inputs)
        s2, p2 = self.e2(p1)
        s3, p3 = self.e3(p2)
        s4, p4 = self.e4(p3)

        # Bottleneck
        b = self.b(p4)

        # Decoder
        d1 = self.d1(b, s4)
        d2 = self.d2(d1, s3)
        d3 = self.d3(d2, s2)
        d4 = self.d4(d3, s1)

        # Classifier
        outputs = self.outputs(d4)

        return outputs

import requests
from PIL import Image
import io
import base64
import sys


url = 'http://localhost:8003/invocations'  # Replace with your server URL if different
# Check if an image file path is provided as a command-line argument
if len(sys.argv) < 2:
    print("Please provide the image file path as a command-line argument.")
    sys.exit(1)

image_path = sys.argv[1]

# Open the image in binary mode
with open(image_path, 'rb') as img:
    # Prepare the image data
    image_data = img.read()

    # Create a dictionary to hold the file data
    files = {
        'file': (image_path.split("/")[-1], image_data)
    }

    # Send the POST request
    response = requests.post(url, files=files)

    # Handle the response
    if response.status_code == 200:
        # If successful, process the base64 image string
        image_string = response.json()['image']
        
        # Convert base64 string back into bytes
        img_data = base64.b64decode(image_string)

        # Convert bytes data into a PIL Image object
        img = Image.open(io.BytesIO(img_data))

        # Now you can do anything you want with the image object
        # For example, let's show it
        img.save('output.png', 'PNG')

    else:
        print('Request failed with status code', response.status_code)
        print('Response:', response.text)  # print the response text for debugging
import pandas as pd
import numpy as np
import mlflow
import os 
from dotenv import load_dotenv
from fastapi import FastAPI, File, UploadFile, HTTPException

import logging
import torch
import torch.nn as nn
import torchvision
import typer
from pathlib import Path
import torchvision.transforms as transforms
from PIL import Image
from torchvision.transforms.functional import to_pil_image

import io
import base64
from fastapi.responses import JSONResponse
from unet import ResidualUnet


# Load env variables
load_dotenv()

# Initialize the FastAPI application
app = FastAPI()

# Create class to store deployed model
class Model:
    def __init__(self, model_name, model_stage):
        # Load model from Registry
        self.model = mlflow.pytorch.load_model(f"models:/{model_name}/{model_stage}")

    def predict(self, data):
        with torch.no_grad():
            predictions = self.model(data)
        return predictions
    
def transform_image():
    transform = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
            ])

# Create model
model = Model("model_0306_v0", "Staging")

# Create POST endpoint with path ...
@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith(".jpg") or file.filename.endswith(".png") or file.filename.endswith(".jpeg"):
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        image = Image.open(file.filename)
        os.remove(file.filename)

        transform = transforms.Compose(
            [
                transforms.Resize((32, 32)),
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
            ]
        )
        transform_img = transform(image)
        transform_img = transform_img.unsqueeze(0)

        model_pred = model.predict(transform_img)
        model_pred = model_pred.squeeze()
        model_pred = model_pred * 0.5 + 0.5 
        model_pred = torch.clamp(model_pred, 0, 1)
        output_image = to_pil_image(model_pred)

        byte_arr = io.BytesIO()
        output_image.save(byte_arr, format='PNG')

        # Convert to base64
        encoded_image = base64.b64encode(byte_arr.getvalue()).decode()

        return JSONResponse(content={'image': encoded_image})

        return output_image
    else:
        raise HTTPException(status_code=400, detail="Invalid format. Only .png or .jpg accepted.")

if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    exit(1)